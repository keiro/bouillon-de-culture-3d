class Bacille extends Cellule{
  
  //Constructeur
  Bacille(){
    
    //Diamètre min/max
    fMinRadius = 20;
    fMaxRadius = 20;
    
    //la longueur est géré via le scale dans la classe Cellule.
    // ce serait mieux de le définir ici...
    
    //Code couleur
    iRed = 255;
    iGreen = 140;
    iBlue = 0;
    
  }
  
}
