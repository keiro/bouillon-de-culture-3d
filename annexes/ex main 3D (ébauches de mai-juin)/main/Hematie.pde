class Hematie extends Cellule{
  
  //Constructeur
  Hematie(){
    
    //Diamètre min/max
    fMinRadius = 60;
    fMaxRadius = 80;
    
    //Code couleur
    iRed = 255;
    iGreen = 0;
    iBlue = 0;
    
  }
  
}
