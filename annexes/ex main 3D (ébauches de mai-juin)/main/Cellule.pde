abstract class Cellule {

  
  //utilisé pour la définition des proportions lors du display
  float echelle[] = {1.0, 1.0, 1.0};
  //Ellipsoid bacille;
  
  //Calcul de coordonnées aléatoire pour le placement des bactéries.
  //TODO : gestion des collisions
  void setRandCoords(){
    fRadius = random(fMinRadius,fMaxRadius);
    fx = random(0+(fRadius*2),SCENE_LARGEUR-(fRadius*2));
    fy = random(0+(fRadius*2),SCENE_HAUTEUR-(fRadius*2));
    fz = random(-SCENE_PROFONDEUR,SCENE_PROFONDEUR);
  }
  
  //Affichage
  void display(){
    
    //Calcul de coordonnées aléatoires et de l'élément
    setRandCoords();
    
    //Aspect visuel
    noStroke();
    fill(iRed,iGreen,iBlue);
    lights();
    
    //Positionnement et affichage
    pushMatrix();
    translate(fx,fy,fz);
    

    //si on a un bacille, on l'allonge.
    if(this instanceof Bacille)
    {             
      //longueur entre x1.2 et x4 le diametre défini 
      longueurBacille = random(1.2,4);
      
      //définition de l'axe sur lequel l'allongement va se faire : x y ou z
      int position = (int)random(1,3);
      echelle[position] = longueurBacille;
      
      //définition de l'allongement
      scale( echelle[0],echelle[1],echelle[2] ); 
    } 
    else {  
     //les autres éléments restent sphériques
      scale(1,1,1);
    }
    
    
    sphereDetail(10,80);
    sphere(fRadius);
    popMatrix();
  }
  
}
