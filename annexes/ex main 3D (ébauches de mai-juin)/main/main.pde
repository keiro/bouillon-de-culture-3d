//import processing.opengl.*;
import shapes3d.*;
import peasy.*;

//définition de la caméra
PeasyCam cam;

//Définition des classes "éléments graphiques"
Coque oCoque;
Hematie oHematie;
Bacille oBacille;

//variable globales de définition de la scène
static final int SCENE_LARGEUR = 1200, SCENE_HAUTEUR = 750, SCENE_PROFONDEUR = 400;

float fx, fy, fz;

float fMinRadius, fMaxRadius, fRadius, longueurBacille; //tailles des éléments
int iRed, iGreen, iBlue;
float nbCoques, nbBacilles, nbHematies, nbBacterie;
int screenWidth, screenHeight;




void setup(){
  
  size(SCENE_LARGEUR,SCENE_HAUTEUR,P3D);
  //smooth(); //c'est déjà appellé par défaut
  
  nbCoques = random(10,40);
  nbBacilles = random(10,20);
  
  nbBacterie = nbCoques + nbBacilles;
  nbHematies = nbBacterie/20; //on génère 20x moins d'hématie que de bactéries
  
  for(int i = 0; i<nbCoques; i++){
    
    //Display original des coques
    oCoque = new Coque();
    oCoque.display();

  }

  for(int i = 0; i<nbHematies; i++){

    //Display original des hématies
    oHematie = new Hematie();
    oHematie.display();
  }
  
  for(int i = 0; i<nbBacilles; i++){
    //Display original des bacilles
    oBacille = new Bacille();
    oBacille.display();
  }
  
}

void draw(){  
}

//Zoom in / Zoom out
void mouseWheel(MouseEvent event){
  
  if(event.getCount()>0){
    println("ZOOM IN");
  }
  
  if(event.getCount()<0){
    println("ZOOM OUT");
  }
  
  println("Mousewheel : "+event.getCount());
}

void keyPressed(){
  switch(keyCode){
    default :
    case 37:
      println("ROTATE LEFT");
    break;
    case 38:
      println("ROTATE UP");
    break;
    case 39 :
      println("ROTATE RIGHT");
    break;
    case 40 :
      println("ROTATE DOWN");
    break;
  }
}



