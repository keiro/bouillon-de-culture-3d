# README #

Pour récupérer le repo :
git clone git@bitbucket.org:keiro/bouillon-de-culture-3d.git

### Objectif ###

**v1.0 FR**
Bouillon de culture de cellules. Le choc des cellules d’un type particulier créé un son précis. L’utilisateur peut gérer la vitesse de  la scène (pause, ou déplacement des cellules via un slider, la voix) et se déplacer dans la scène. Projet à but pédagogique, d’éveil sur les interactions microscopiques.

Projet réalisé dans le cadre d'une licence informatique CNAM (Unité NSY116)

### Environnement de développement initial ###

* Processing 2.2.1
* Windows