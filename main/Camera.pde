/**
 Gestion de la caméra et évènement associés
 */

//Déclaration objet caméra
PeasyCam cam;

//Objet choisi via le clic sur lequel on va se centrer
Shape3D picked = null;

//distance entre caméra et point observé
float DISTANCE_USUELLE = 600;

/**
 Initialise la caméra lors du lancement de l'application
 */
void camInit() {

  //paramétres initiaux de la caméra
  cam = new PeasyCam(this, 0, 0, 0, DISTANCE_USUELLE);
  cam.setResetOnDoubleClick(false); 
  cam.setMinimumDistance(100);
  cam.setMaximumDistance(2000);
}

/**
 fixe la caméra sur la cellule passée en paramètre
 */
void camLookAt(Shape3D cellule, float distance) {
  cam.lookAt(cellule.getPosArray()[0], cellule.getPosArray()[1], cellule.getPosArray()[2], distance);
}

/**
 fixe la caméra pour regarder le centre de la scène
 */
void camCenter() {
  cam.lookAt(0, 0, 0, DISTANCE_USUELLE);
}

/**
 Gestion du clic gauche de souris. Impact sur la caméra :
 2 clics gauche : caméra se place sur l'lélément cliqué
 1 clic droit : la caméra reprends sa position initiale (au démarrage de l'application)
 */
void mouseClicked() {

  //on repère si il y a une forme là où on clique
  picked = Shape3D.pickShape(this, mouseX, mouseY);

  //si double clic sur une cellule, on la centre
  if (mouseButton == LEFT && mouseEvent.getClickCount()==2 && picked != null) camLookAt(picked, 300);

  //si clic droit, on réinitialise la caméra sur le milieu de la scène
  if (mouseButton == RIGHT) cam.reset();
}

