/**
 Class Hematie, hérite de Cellule
 */
 
 class Hematie extends Cellule {

  /**
   Constructeur
   @param obj Applet du main
   */
  Hematie(PApplet obj) {

    //Création de la cellule
    super(obj);
    circleRadius = random(60, 80);//entre 6 et 8 µm

    //définition de sa taille
    myCell.setRadius(circleRadius, circleRadius, circleRadius);

    //définition de sa couleur
    couleurOriginale = #ED1D2E;
    myCell.fill(couleurOriginale);
  }
}
