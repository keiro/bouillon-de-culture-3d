/**
 Class Onde
 Génère une onde lorsqu'une cellule entre en collision
 */

class Onde {

  Ellipsoid myWave;
  private float posX, posY, posZ, circleRadius;
  private int nbOccurences = 0;

  /**
   Constructeur
   Défini l'emplacement de l'onde de choc
   @param obj Applet du main
   @param x, y, z, rad position et radius de la cellule en collision
   */
  Onde(PApplet obj, float x, float y, float z, float rad) {
    
    myWave = new Ellipsoid(obj, 50, 50);
    
    // définition de sa position initiale dans la scène
    posX = x;
    posY = y;
    posZ = z;
    circleRadius = rad;
    
    // on place l'onde aux coordonnées de la cellule provoquant la collision
    myWave.moveTo(posX, posY, posZ);
    myWave.setRadius(circleRadius, circleRadius, circleRadius);
  }
  
  /**
  Affichage de l'onde de choc après une collision
 */
  void display() {
    
  // une sphère blanche représente l'onde de chocs pendant TAILLE_ONDEDECHOC (cf. param du main) draw
    if (nbOccurences < TAILLE_ONDEDECHOC) {
      if (!pauseOn) {
        
        //si on est pas en pause, on agrandi l'onde choc
        myWave.setRadius(circleRadius++, circleRadius++, circleRadius++);
        nbOccurences++;
      }
      
      //affichage de la sphere blanche et transparente à 60%
      myWave.fill(color(#FFFFFF, 60));
      myWave.draw();
      
    } else {
      
      //on supprime l'objet de la liste une fois passé les TAILLE_ONDEDECHOC itérations
      listeOndesChocs.remove(this);
    }
  }
}

