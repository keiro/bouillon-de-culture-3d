/**
 Class Coque, hérite de Cellule
 */

class Coque extends Cellule {

  //définition d'un tableau avec les couleurs possibles :
  int []tabCoqueCouleurs = {#EAA3D3, #B73D8E, #6F0C4E}; //gamme de mauve
  int indexCol; // var temp utilisée dans le choix de la couleur aléatoire (meilleure visibilité du code)

  /**
   Constructeur
   @param obj Applet du main
   */
  Coque(PApplet obj) {

    //Création de la cellule
    super(obj);
    circleRadius = random(8, 16);//entre 0.8 et 1.6µm

    //définition de sa taille
    myCell.setRadius(circleRadius, circleRadius, circleRadius);

    //définition de sa couleur
    indexCol = floor(random(0, tabCoqueCouleurs.length));
    couleurOriginale = tabCoqueCouleurs[indexCol];
    myCell.fill(couleurOriginale);
  }
}

