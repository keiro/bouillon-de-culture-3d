/**
 
 **************
  
 @version 1.0
 @author 2015-07 Monteiro Kévin - keiro.dev@gmail.com
 
 Bouillon de culture
 
 **************
 
 
 Quelques règles :
 
 - Après 10 chocs, les hématies éclatent (pas les autres cellules)
 - Son différents pour chaque collision entre types de cellule, et de couleurs
 - Les collisions provoquent une onde de choc
 - Double clic sur une cellule nous centre sur celle-ci
 - Possibilité d'accélérer l'animation via le slider (via souris)
 - Possibilité d'accélérer l'animation via le colume de l'ambiance sonore (clic, ou touche V)
 - Possibilité de play / pause de l'animation (touche P ou espace)
 - Possibilité de relancer l'animation avec la quantité choisir de chaque cellules (Touche Entrée)
 - Déplacement : 
 * zoom, dézoom avec molette
 * clic + mouvement pour faire une rotation de la scène
 * triple clic pour revenir au point de départ (ou touche C)
 
 Rappel raccourcis clavier/souris :
 - P / Espace : Play / Pause
 - Entrée : relancer l'animation avec les paramètres voulus
 - C / Clic droit : centrer caméra à l’origine de la scène
 - V : Active/Désactive l’entrée Audio
 - Double clic gauche sur un élément : se rapprocher de cet élément
 - Tabulation : déplacement entre les champs texte des paramètres
 
 
 
 Caratéristiques techniques :
 - Echelle suivante : 1µm réel = 10px processing;
 - génération de 3 éléments différentes :
 * coques, rond, 3 couleurs possibles, entre 0.8 et 1.5µm de diamètres
 * bacilles, ellipsoid, 3 couleurs possibles, entre 1,5 et 6 µm de long
 * hématies, rond, rouge, entre 7 et 9µm de diamètre
 - toutes les captures de touches ont été testées sous Windows 8, mais aussi sous Mac Pro
 
 Panel :
 - Impossible de mettre autre chose qu'un nombre dans le panel paramètre de l'animation
 - Limité à 70 éléments maximum
 - possibilité de tabulation entre les différents champs (tab = avancer, ou shift + tab = reculer)
 - possibilité de Dragging du panel paramètre via sa barre supérieure
 
 
 
 Bugs connus : 
 - Lors de la mise en pause, la rotation des bacilles éventuellement engagé continue
 (car elle se déroule sur 3 secondes)
 - la GUI subit la lumière ambiante (non corrigé volontairement car ça donne un coté dynamique intéressant)
 - la scène subit le drag de la souris sur la GUI
 
 
 
 
 => Développé avec Processing 2.2.1 sous Windows 64bits
 
 
 */

// références des librairies utilisées :
// doc cam | http://mrfeinberg.com/peasycam/
// shape3d | http://www.lagers.org.uk/s3d4p/ref/index.html
// G4P | http://lagers.org.uk/g4p/ref/index.html
// minim | http://code.compartmental.net/minim/

//import processing.opengl.*; //au choix OPENGL ou P3D
import peasy.*; //caméra
import shapes3d.*; //formes
import g4p_controls.*; //GUI
import ddf.minim.*;  //Son

// Stockage des éléments
ArrayList<Cellule> listeDesCellules;
ArrayList<Onde> listeOndesChocs;

// Gestion du play & pause (play activé par défaut)
boolean pauseOn = false;
//gestion de l'écoute de l'audio (désactivé par défaut)
boolean audioOn = false;




/* ***************************************************
 
 //  ****** Variables de paramétrages globaux ********
 
 ************************************************** */


// Dimensions de la scène
static final int SCENE_LARGEUR = 1200, SCENE_HAUTEUR = 800;

int sceneLimites = floor(SCENE_HAUTEUR/1.5); //espace de déploiement initial des éléments

int nbMaxElements = 70; //au dessus les performances sont ralentis 

//quantité de chaque cellule initialement
int nbBacilles = 20;
int nbCoques = 20;
int nbHematies = 5;

int TAILLE_ONDEDECHOC = 10; //si moins on ne la voit pas assez, si plus peu faire ramer certains processeur




/* ***************************************************
 
 *****************************************************
 
 ************************************************** */


void setup() {

  size(SCENE_LARGEUR, SCENE_HAUTEUR, P3D);

  createGUI(); //Création de la graphic user interface (GUI)

  camInit(); //paramétrage de la caméra

  initialisationSons(); //définition des sons

  initialisation(); //Génération de l'animation
}

/**
 Création des cellules (éléments graphiques principaux)
 */
void initialisation() {

  //création de la liste des cellules et des ondes de chocs
  listeDesCellules = new ArrayList<Cellule>(nbCoques+nbHematies+nbBacilles);
  listeOndesChocs = new ArrayList<Onde>();

  //création des différentes cellules à des espaces sans collisions
  for (int i = 0; i < nbCoques + nbHematies + nbBacilles; i++) {

    //pour arg 2 : 0 = coque, 1 hématie,  2 bacille
    if (i >= 0 && i < nbCoques) {
      createCoordosDispo(i, 0);
    } else if (i >= nbCoques && i < nbCoques + nbHematies ) {
      createCoordosDispo(i, 1);
    } else if (i >= nbCoques + nbHematies) {
      createCoordosDispo(i, 2);
    }
  }
}

/**
 Fonction récursive pour trouver des coordonnées disponibles lors de la création de l'élément (disponible signifie ici "sans collision")
 @param i index de la cellule dans la listeDesCellules
 @param typeCellNum Type de cellule (0 coque, 1 hématie, 2 bacille
 */
void createCoordosDispo (int i, int typeCellNum) {

  boolean continuer = true;

  // en fonction du type reçu, on crée la cellule nécessaire
  // type : 0 coque, 1 hématie, 2 bacille
  switch(typeCellNum) {
  case 0:
    listeDesCellules.add(i, new Coque(this));
    break;
  case 1:
    listeDesCellules.add(i, new Hematie(this));
    break;
  case 2:
    listeDesCellules.add(i, new Bacille(this));
    break;
  }

  for (int j = 0; j < i; j++) {
    //on regarde si il y a une intersection entre l'élément tout juste créé et un autre élément déjà initialisé
    if (continuer && 
      circleCircleIntersect(listeDesCellules.get(i).getPosition()[0], listeDesCellules.get(i).getPosition()[1], listeDesCellules.get(i).getPosition()[2], listeDesCellules.get(i).getCircleRadius(), 
    listeDesCellules.get(j).getPosition()[0], listeDesCellules.get(j).getPosition()[1], listeDesCellules.get(j).getPosition()[2], listeDesCellules.get(j).getCircleRadius())) {

      // si oui, on retire cette cellule non valide 
      listeDesCellules.remove(i);
      // puis on sort de cette boucle
      continuer = false;
      // et on relance des coordonnées aléatoires pour cette cellule
      createCoordosDispo(i, typeCellNum);
    }
  }
}

/** **********************

      Loop principal

 ********************* */
void draw () {

  clear();
  background(#06263B);
  lights();

  //Contrôle de la vitesse avec le son ambiant
  speedViaVoice();

  pushMatrix();
  //animation des cellules
  for (int i = 0; i < listeDesCellules.size (); i++) {

    //déplacement brownien des cellules
    listeDesCellules.get(i).move();

    // seul les bacilles peuvent pivoter
    if (listeDesCellules.get(i) instanceof Bacille) listeDesCellules.get(i).tourne();

    //affichage des cellules
    listeDesCellules.get(i).display(); 

    //on supprime les Hématies ayant plus de 10 chocs
    if (listeDesCellules.get(i) instanceof Hematie && listeDesCellules.get(i).getNbChocs() >= 10)
      listeDesCellules.remove(i);
  }

  // animation des ondes de chocs
  for (int i = 0; i < listeOndesChocs.size (); i++) {
    listeOndesChocs.get(i).display();
  }
  popMatrix();
}

/**
 Renvoi si on a une intersection ou non entre les 2 coordonnées 3D envoyées en paramètres
 @param cx1, cy1, cz1 coordonnées xyz de la cellule 1
 @param cr1 radius de la cellule 1
 @return true si intersaction entre les 2 sets de coordonnées
 */
boolean circleCircleIntersect(float cx1, float cy1, float cz1, float cr1, float cx2, float cy2, float cz2, float cr2) {
  return (dist(cx1, cy1, cz1, cx2, cy2, cz2) < cr1 + cr2) ? true : false;
}

/**
 Retourne une valeur comprise entre -num et +num
 Fonction utilisée pour la lisibilité/clarté du code
 @param num Float souhaité
 @return Une valeur aléatoire entre -num et + num
 */
float generRandom (float num) {
  return random(-num, num);
}

/**
 Gestion des touches pressées
 */
void keyPressed () {

  //P et Espace play / pause
  if (key == 'p' || key == 'P' || key == 32)   //32 = barre d'espace
  {
    //Play/pause
    if (!pauseOn) btnPause_gestion();
    else btnPlay_gestion();
  }

  //Touche V | slider modifié via la voix, qui contrôle donc la vitesse
  if (key == 'v' || key == 'V')
  {
    //coche ou décoche le bouton suivi son statut actuel
    if (TGL_voice.stateValue() == 1) { 
      TGL_voice.setState(0);
    } else {
      TGL_voice.setState(1);
    }
    //traitement de l'état du bouton
    TGL_voice_gestion();
  }

  if (keyCode == 10)   //10 = Entrée fonctionnant sur Windows / Mac
  {
    //Équivaut au clic pour relancer l'animation
    btnReload_gestion();
  }

  //Touche C | recentre la caméra pour regarder le milieu de la scène
  if (key == 'c' || key == 'C') camCenter();
}

