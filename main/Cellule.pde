/**
 Classe Cellule abstraite
 */

abstract class Cellule {

  protected Ellipsoid myCell;
  protected float vitesse;
  protected PApplet obj;//applet du main pour utilisation apr la librairie Shapes3D

  protected float[] tabPositionThisCell = new float[3]; //stocke position actuelle de la cellule
  protected float[] tabPreviousPos = new float [3]; //stocke la position précédente
  protected float circleRadius; //diametre max de la cellule
  protected int nbChocs = 0; //si supérieur à 10, on explose la cellule (hématie seulement)
  protected int couleurOriginale;

  private boolean collision;

  /**
   Constructeur
   @param obj Applet du main
   */
  Cellule(PApplet applet) {
    this.obj = applet;
    //création de la cellule
    myCell = new Ellipsoid(obj, 50, 50);
    //définition de sa position initiale dans la scène
    myCell.moveTo(random(-sceneLimites, sceneLimites), random(-sceneLimites, sceneLimites), random(-sceneLimites, sceneLimites));
  }

  /**
   Défini de nouvelles coordonnées à la cellule
   */
  void setCoordos() {

    //implémentation de la vitesse (si pause ou non...)
    vitesse = (pauseOn) ? 0.0 : sliderVitesse.getValueF();

    //création random de position aléatoire entre -vitesse et +vitesse
    tabPositionThisCell[0] = constrain(myCell.getPosArray()[0]+generRandom(vitesse), -SCENE_LARGEUR/2, SCENE_HAUTEUR/2);
    tabPositionThisCell[1] = constrain(myCell.getPosArray()[1]+generRandom(vitesse), -SCENE_LARGEUR/2, SCENE_HAUTEUR/2);
    tabPositionThisCell[2] = constrain(myCell.getPosArray()[2]+generRandom(vitesse), -SCENE_LARGEUR/2, SCENE_HAUTEUR/2);

    //déplacement de la cellule aux dites positions
    myCell.moveTo(tabPositionThisCell[0], tabPositionThisCell[1], tabPositionThisCell[2]);
  }

  /**
   Gère le déplacement puis la collision éventuelle d'une cellule
   */
  void move() {

    //déplacement à de nouvelles coordonnées
    this.setCoordos();

    //bool de vérification si collision avec d'autres éléments
    collision = false;

    //on parcours chaque cellule pour chercher si il y a une collision 
    for (int i = 0; i < listeDesCellules.size (); i++) {

      // SI (on est pas sur l'élément actuel) ET (il y a collision)
      // circleCircleIntersect(ELEMENT dans la liste, object actuel)
      if ( (listeDesCellules.get(i).getPosition()[0] != this.getPosition()[0] && listeDesCellules.get(i).getPosition()[1] != this.getPosition()[1] && listeDesCellules.get(i).getPosition()[2] != this.getPosition()[2]) && 
        circleCircleIntersect(listeDesCellules.get(i).getPosition()[0], listeDesCellules.get(i).getPosition()[1], listeDesCellules.get(i).getPosition()[2], listeDesCellules.get(i).getCircleRadius(), 
      this.getPosition()[0], this.getPosition()[1], this.getPosition()[2], this.getCircleRadius()))
      {
        //on a une collision : bruit
        if (this instanceof Coque && listeDesCellules.get(i) instanceof Coque) {
          //println("coque - coque");
          cc.rewind();
          cc.play();
        } else if (this instanceof Hematie && listeDesCellules.get(i) instanceof Hematie) {
          //println("Hematie - hematie");
          hh.rewind();
          hh.play();
        } else if ( (this instanceof Coque && listeDesCellules.get(i) instanceof Hematie) || 
          (this instanceof Bacille && listeDesCellules.get(i) instanceof Hematie) ||
          (this instanceof Hematie && listeDesCellules.get(i) instanceof Coque) ||
          (this instanceof Hematie && listeDesCellules.get(i) instanceof Bacille) ) {
          //println("Bactérie - Hematie");
          hcb.rewind();
          hcb.play();
        } else if (  (this instanceof Coque && listeDesCellules.get(i) instanceof Bacille && listeDesCellules.get(i).couleurOriginale == #AEDE8D) ||
          (this instanceof Bacille && this.couleurOriginale == #AEDE8D && listeDesCellules.get(i) instanceof Coque)) {
          //println("Coque - Bacille | son 1");
          cb1.rewind();
          cb1.play();
        } else if (  (this instanceof Coque && listeDesCellules.get(i) instanceof Bacille && listeDesCellules.get(i).couleurOriginale == #AEDE8D) ||
          (this instanceof Bacille && this.couleurOriginale == #83CB50 && listeDesCellules.get(i) instanceof Coque)) {
          //println("Coque - Bacille | son 2");
          cb2.rewind();
          cb2.play();
        } else if (  (this instanceof Coque && listeDesCellules.get(i) instanceof Bacille && listeDesCellules.get(i).couleurOriginale == #AEDE8D) ||
          (this instanceof Bacille && this.couleurOriginale == #4B9815 && listeDesCellules.get(i) instanceof Coque)) {
          //println("Coque - Bacille | son 3");
          cb3.rewind();
          cb3.play();
        } else    if (this instanceof Bacille && listeDesCellules.get(i) instanceof Bacille) {
          //println("Bacille - Bacille");
          bb.rewind();
          bb.play();
        } 

        //rétablie les coordonnées actuelles de la cellule comme étant les précédentes (donc sans collision)
        myCell.moveTo(tabPreviousPos[0], tabPreviousPos[1], tabPreviousPos[2]);
        collision = true;

        //affichage d'un point d'impact radial
        listeOndesChocs.add(new Onde(obj, this.getPosition()[0], this.getPosition()[1], this.getPosition()[2], this.getCircleRadius()));

        if (this instanceof Hematie) nbChocs();//incrémente le nombre de chocs de l'hématie
      }
    }

    //stockage des coordonnées actuelles sans collision
    tabPreviousPos[0] = tabPositionThisCell[0];
    tabPreviousPos[1] = tabPositionThisCell[1];
    tabPreviousPos[2] = tabPositionThisCell[2];
  }

  /**
   Incrémente nbChocs de 1 (utilisé uniquement pour faire éclater les hématies
   */
  void nbChocs() {
    this.nbChocs++;
  }

  /**
   Getter du nombre de chocs
   @return nbChocs
   */

  int getNbChocs() {
    return this.nbChocs;
  }

  /**
   Retourne la position actuelle de l'objet
   @return la position de la cellule actuelle float[x,y,z]
   */
  float[] getPosition() {
    return myCell.getPosArray();
  }

  /**
   Getter de circleRadius
   @return circleRadius de la cellule
   */
  float getCircleRadius() {
    return this.circleRadius;
  }

  /**
   Affichage de la cellule
   */
  void display() {   
    myCell.draw();
  }

  /**
   déclaré pour pouvoir être utilisé par la classe bacille
   */
  void tourne() {
  };
}

