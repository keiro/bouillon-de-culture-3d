/**
 Gestion de la Graphic user Interface
 Vitesse de l'animation de 1 à 6 (si >6, ça "vibre" trop)
 */

//Déclaration des objets pour la GUI
GSlider sliderVitesse;
GImageToggleButton TGL_voice;
GLabel LB_toggleV;
GLabel LB_toggleInfo;

GButton btnPlay; 
GButton btnPause; 

GPanel panelSetup; 
GLabel LB_nbCoques;
GLabel LB_nbBacilles;
GLabel LB_nbHematies;
GLabel LB_info;

GTextField TXT_nbCoques;
GTextField TXT_nbBacilles;
GTextField TXT_nbHematies;

GButton btnReload;

GTabManager tabulation;

/**
 Crée les différents panel, boutons, slider et texte et donne un titre à la fenêtre
 */
public void createGUI() {
  G4P.messagesEnabled(false);
  G4P.setGlobalColorScheme(GCScheme.CYAN_SCHEME);
  G4P.setCursor(ARROW);
  if (frame != null)  
    frame.setTitle("Projet CNAM - Bouillon de culture");

  //Création du slider
  sliderVitesse = new GSlider(this, 40, 300, 162, 80, 20.0); //20 => largeur du sélecteur
  sliderVitesse.setShowValue(true);
  sliderVitesse.setShowLimits(true);
  sliderVitesse.setTextOrientation(G4P.ORIENT_RIGHT);
  sliderVitesse.setRotation(radians(270), GControlMode.CORNER);
  sliderVitesse.setLimits(2, 1, 6);
  sliderVitesse.setNbrTicks(6);
  sliderVitesse.setStickToTicks(true);
  sliderVitesse.setShowTicks(true);
  sliderVitesse.setEasing(2.0);
  sliderVitesse.setNumberFormat(G4P.INTEGER, 0);
  sliderVitesse.setOpaque(true);

  //Création du bouton play (masqué)
  btnPlay = new GButton(this, 40, 90, 80, 30);
  btnPlay.setText("Play");
  btnPlay.setTextBold();
  btnPlay.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  btnPlay.addEventHandler(this, "btnPlay_click");
  //masquage du bouton play à l'initialisation
  btnPlay.setEnabled(false);
  btnPlay.setVisible(false);

  //Création du bouton pause
  btnPause = new GButton(this, 40, 90, 80, 30);
  btnPause.setText("Pause (P/Espace)");
  btnPause.setTextBold();
  btnPause.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  btnPause.addEventHandler(this, "btnPause_click");

  //création du toggle voice
  TGL_voice = new GImageToggleButton(this, 50, 315, "imgs/btnAudio.png", 2, 1);
  TGL_voice.addEventHandler(this, "TGL_voice_click");

  LB_toggleV = new GLabel(this, 60, 381, 41, 20);
  LB_toggleV.setOpaque(true);
  LB_toggleV.setText("(V)");
  LB_toggleV.setTextBold();

  LB_toggleInfo = new GLabel(this, 20, 405, 120, 40);
  LB_toggleInfo.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  LB_toggleInfo.setOpaque(true);
  LB_toggleInfo.setText("Le bruit ambiant influence la vitesse");
  LB_toggleInfo.setVisible(false); //invisible par défaut vu que l'acquisition audio est coupée

    //Création du panel paramètres
  panelSetup = new GPanel(this, width-170, height-210, 140, 195, " ___  Paramètres  ___ ");
  panelSetup.setOpaque(true);

  //Boutons du panel paramètres :
  LB_nbCoques = new GLabel(this, 5, 26, 80, 20);
  LB_nbCoques.setText("Coques voulu");
  LB_nbCoques.setOpaque(false);
  TXT_nbCoques = new GTextField(this, 100, 26, 30, 20, G4P.SCROLLBARS_NONE);
  TXT_nbCoques.setText(""+nbCoques);

  LB_nbBacilles = new GLabel(this, 5, 52, 80, 20);
  LB_nbBacilles.setText("Bacilles voulu");
  LB_nbBacilles.setOpaque(false);
  TXT_nbBacilles = new GTextField(this, 100, 52, 30, 20, G4P.SCROLLBARS_NONE);
  TXT_nbBacilles.setText(""+nbBacilles);

  LB_nbHematies = new GLabel(this, 5, 78, 90, 20);
  LB_nbHematies.setText("Hématies voulu");
  LB_nbHematies.setOpaque(false);
  TXT_nbHematies = new GTextField(this, 100, 78, 30, 20, G4P.SCROLLBARS_NONE);
  TXT_nbHematies.setText(""+nbHematies);

  TXT_nbCoques.addEventHandler(this, "TXT_change");
  TXT_nbBacilles.addEventHandler(this, "TXT_change");
  TXT_nbHematies.addEventHandler(this, "TXT_change");

  //Création du bouton pause
  btnReload = new GButton(this, 20, 140, 100, 40);
  btnReload.setText("Recharger (Entrée)");
  btnReload.setTextBold();
  btnReload.addEventHandler(this, "btnReload_click");


  LB_info = new GLabel(this, 5, 100, 130, 40);
  LB_info.setLocalColorScheme(GCScheme.RED_SCHEME);


  // liaison et intégration dans le panel de droite
  panelSetup.addControl(LB_nbCoques);
  panelSetup.addControl(LB_nbBacilles);
  panelSetup.addControl(LB_nbHematies);

  panelSetup.addControl(TXT_nbCoques);
  panelSetup.addControl(TXT_nbBacilles);
  panelSetup.addControl(TXT_nbHematies);

  panelSetup.addControl(LB_info);
  panelSetup.addControl(btnReload);

  //tabulation entre les éléments du panel
  tabulation = new GTabManager();
  tabulation.addControls(TXT_nbCoques, TXT_nbBacilles, TXT_nbHematies);
}

/**
 Evènement déclenché si clic sur le bouton PLAY
 @param Bouton cliqué, type d'évènement
 */
public void btnPlay_click(GButton source, GEvent event) {
  btnPlay_gestion();
}

/**
 Gestion des actions lors du clic sur le bouton Play
 */
public void btnPlay_gestion() {
  //désactive et masque le btn play
  btnPlay.setEnabled(false);
  btnPlay.setVisible(false);

  //active et affiche le btn pause
  btnPause.setEnabled(true);
  btnPause.setVisible(true);

  pauseOn = false;
}

/**
 Evènement déclenché si clic sur le bouton Pause
 @param Bouton cliqué, type d'évènement
 */
public void btnPause_click(GButton source, GEvent event) {
  btnPause_gestion();
}

/**
 Gestion des actions lors du clic sur le bouton Pause
 */
public void btnPause_gestion() {
  btnPause.setEnabled(false);
  btnPause.setVisible(false);

  btnPlay.setEnabled(true);
  btnPlay.setVisible(true);

  pauseOn = true;
}

/**
 Evènement déclenché si clic sur le bouton toggle de voix ON/OFF
 @param Bouton cliqué, type d'évènement
 */
public void TGL_voice_click(GImageToggleButton source, GEvent event) {
  TGL_voice_gestion();
}

/**
 Gestion des actions lors du clic sur le bouton toggle de voix ON/OFF
 */
public void TGL_voice_gestion () {
  switch(TGL_voice.stateValue()) {


  case 0: // on DÉSactive l'acquisition audio

    LB_toggleInfo.setVisible(false); // masque le message d'info
    sliderVitesse.setEnabled(true); // activation du slider
    audioOn = false; // désactivation de l'objet d'acquisition sonore
    break;

  case 1: // on ACTIVE l'acquisition audio
    LB_toggleInfo.setVisible(true); // affiche le message d'info
    sliderVitesse.setEnabled(false); // désactive le slider
    audioOn = true; // active l'acquisition sonore
    break;
  }
}

/**
 Evènement déclenché sur modification d'une des valeurs des champs texte (paramètres)
 On vérifie si les données sont des INTEGER dont la somme est inférieur à nbMaxElements
 @param Bouton cliqué, type d'évènement
 */
public void TXT_change(GTextField source, GEvent event) {

  /*
  Si impossible de convertir en INT, c'est que l'user n'a pas entré un INT, donc on désactive le bouton pour recharger l'animation.
   Si c'est bien des INT, on l'active.
   //abs pour éviter les nombres négatifs
   */
  try {
    nbCoques = abs(Integer.parseInt(TXT_nbCoques.getText()));
    nbHematies = abs(Integer.parseInt(TXT_nbHematies.getText()));
    nbBacilles =  abs(Integer.parseInt(TXT_nbBacilles.getText()));

    if (nbCoques + nbHematies + nbBacilles > nbMaxElements) {
      //message erreur
      LB_info.setText("Moins de "+nbMaxElements+" cellules svp");
      btnReload.setEnabled(false);
    } else {
      LB_info.setText("");
      btnReload.setEnabled(true);
    }
  } 
  catch (NumberFormatException e) {
    btnReload.setEnabled(false);
  }
} 

/**
 Evènement déclenché lors du clic sur le bouton Relancer
 @param Bouton cliqué, type d'évènement
 */
public void btnReload_click(GButton source, GEvent event) {
  btnReload_gestion();
}

/**
 Gestion des actions lors du clic sur le bouton Relancer
 Replace la caméra et relance l'initialisation de l'appli
 */
public void btnReload_gestion() {
  //re-vérification au cas où l'user est un petit malin.
  try {
    nbCoques = abs(Integer.parseInt(TXT_nbCoques.getText()));
    nbHematies = abs(Integer.parseInt(TXT_nbHematies.getText()));
    nbBacilles =  abs(Integer.parseInt(TXT_nbBacilles.getText()));

    //on reinitialise la caméra
    cam.reset();

    //on relance l'anim
    initialisation();
  } 
  catch (NumberFormatException e) {

    println("Impossible d'arriver ici... en théorie... : " + e);
  }
}

