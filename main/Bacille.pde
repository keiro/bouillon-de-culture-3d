/**
 Class Bacille, hérite de Cellule
 */

class Bacille extends Cellule {

  //définition d'un tableau avec les couleurs possibles :
  int []tabBacilleCouleurs = { #AEDE8D, #83CB50, #4B9815 };//gamme de vert
  int indexCol; // var temp utilisée dans le choix de la couleur aléatoire (meilleure visibilité du code)
  float longueurBacille = random(15, 60);//(entre 1,5 et 6µm)

  /**
   Constructeur
   @param obj Applet du main
   */
  Bacille(PApplet obj) {

    //Création de la cellule
    super(obj);
    circleRadius = longueurBacille/2; //zone ou "sphère" de collision

    //définition de sa position (rotation)
    myCell.rotateTo(random(-1, 1), random(-1, 1), random(-1, 1));
    //définition de sa taille
    myCell.setRadius(circleRadius/2, longueurBacille, circleRadius/2);

    //définition de sa couleur
    indexCol = floor(random(0, tabBacilleCouleurs.length));
    couleurOriginale = tabBacilleCouleurs[indexCol];
    myCell.fill(couleurOriginale);
  }


  /**
   Fait pivoter la cellule, donc seul les bacilles (les autres cellules n'étant pas sphériques)
   */
  void tourne() {
    // si on est pas en pause, 1 chance sur 150 de tourner
    //(à 30fr/sec, ça fait 1 rotation chaque 5sec, sachant que la rotation dure 3.0sec)
    if (!pauseOn && floor(random(0, 149)) == 0) {
      myCell.rotateTo(generRandom(1), generRandom(1), generRandom(1), 3.0, 0.0 );
    }
  }
}

