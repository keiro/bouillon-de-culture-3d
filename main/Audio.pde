/**
 Gestion des initialisation et fonctions liées à l"audio
 */

//Déclarations pour les objets sonores
Minim voiceInMinim;
AudioPlayer bb, cb1, cb2, cb3, cc, hcb, hh;
AudioInput voiceIn;

/**
 Chargement des sons utilisés et de l'entrée audio
 */
void initialisationSons() {
  //définition des sons
  bb = new Minim(this).loadFile("sons/bb.mp3");
  cb1 = new Minim(this).loadFile("sons/cb1.mp3");
  cb2 = new Minim(this).loadFile("sons/cb2.mp3");
  cb3 = new Minim(this).loadFile("sons/cb3.mp3");
  cc = new Minim(this).loadFile("sons/cc.mp3");
  hcb = new Minim(this).loadFile("sons/hcb.mp3");
  hh = new Minim(this).loadFile("sons/hh.mp3");

  //définition entrée audio
  voiceIn = new Minim(this).getLineIn();
}

/**
 Gestion de la prise en compte du son ambiant quand on est pas en pause, et que le bouton AUDIO ON est coché
 */
void speedViaVoice() {

  // si enregistrement audio voulu (et qu'on est pas en pause), 
  // on modifie le sliderVitesse en fonction de l'ambiance sonore
  if (audioOn && !pauseOn) {  
    //abs pour la valeur absolu, 200 pour l'amplification du signal
    sliderVitesse.setValue(constrain(abs(voiceIn.mix.get(1)*200), 1, 6)); //constrain entre 1 et 6
  }
}

